var date = module.exports

date.format = function(date) {
	var date = new Date(date)
	  , month = date.getMonth()
	  , day = date.getDate()
	  , hours = date.getHours()
	  , minutes = date.getMinutes()
	  , ampm = hours >= 12 ? 'pm' : 'am'
	  , strTime

	hours = hours % 12
	hours = hours ? hours : 12
	minutes = minutes < 10 ? '0'+minutes : minutes
	strTime = (month+1) + '/' + day + ' - ' + hours + ':' + minutes + ' ' + ampm

	return strTime

}