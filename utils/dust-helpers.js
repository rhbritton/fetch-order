var currency = require('./currency')
  , date = require('./date')
  , dust = require('consolidate').dust

dust.helpers = require('dustjs-helpers').helpers

dust.helpers.formatCurrency = function(chunk, ctx, bodies, params) {
	var amount = dust.helpers.tap(params.amount, chunk, ctx)
	return chunk.write(currency.format(parseFloat(amount)))
}

dust.helpers.formatDate = function(chunk, ctx, bodies, params) {
	var datetime = dust.helpers.tap(params.datetime, chunk, ctx)
	return chunk.write(date.format(datetime))
}