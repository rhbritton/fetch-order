var express = require('express')
  , app = express()
  , Client = require('node-rest-client').Client
  , order = new Client()
  , config = require('./config')
  , consolidate = require('consolidate')

require('./utils/dust-helpers')

app.use(express.bodyParser())
app.use(express.cookieParser())

app.use('/assets', express.static(process.cwd()+'/assets'))

app.set('views', process.cwd()+'/templates')
app.set('view engine', 'dust')
app.set('view cache', false)
app.engine('dust', consolidate.dust)

order.registerMethod('getOrder', config.api_url+'/orders/${id}', 'GET')
order.registerMethod('getRestaurant', config.api_url+'/restaurants/${id}', 'GET')

app.get('/:id', function(req, res){
	var id = req.params['id']

	order.methods['getOrder']({ path: { id:id } }, function(_order, resp) {
		if(resp.statusCode != 200) return res.end('404')
		
		var remaining = 0

		console.log('1', _order)
		console.log('2', _order.restaurants)

		_order.restaurants.forEach(function(restaurant, i) {
			console.log(restaurant.restaurant)
			remaining++
			console.log(order.methods)
			order.methods['getRestaurant']({ path: { id:restaurant.restaurant } }, function(restaurant, response) {
				_order.restaurants[i].address = restaurant.address
				if(!--remaining) done(false)
			})
		})

		function done(err) {
			if(err) return res.end('404')

			app.render('order', _order, function(err, html) {
				console.log(err)
				res.end(html)
			})
		}
	})
})

app.listen(3000)